variable "amiid" {
  type = string
}
variable "instance_type" {
  type = string
}
variable "sshkey" {
  type = string
}
variable "resource_name" {
  type = string
}
variable "environment_name" {
  type = string
}
