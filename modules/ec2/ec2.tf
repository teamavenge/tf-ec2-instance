resource "aws_instance" "app-server1" {
  ami = var.amiid
  instance_type = var.instance_type
  key_name = var.sshkey
  #subnet_id = "subnet-b16895cc"
  #vpc_security_group_ids = [aws_security_group.app-server1_sg.id]
  tags = {
    Name = var.resource_name
    Env= var.environment_name
  }
}
